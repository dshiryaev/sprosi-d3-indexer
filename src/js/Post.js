/**
 * Один постинг
 */
define(function (require) {
    "use strict";
    /**
     * Создание постинга из jQuery-фрагмента HTML с постингом
     * @param {jQuery} $post
     * @constructor
     */
    var
        /** Внешние зависимости */
        $ = require("jquery"),

        /** Внутренние вспомогательные компоненты */
        POST_TITLE_REGEXP = /\[(.+)\]/im,
        POST_HREF_REGEXP = /sprosi\.d3\.ru\/comments\/[\d]+\//,
        POST_ISVALIDATED_REGEXP = /<span class=\"moderator\">Автор прошел проверку на подлинность<\/span>/i,
        POST_ISSTARRED_REGEXP = /class=\"stars_holder\"/i,

        /** Внутренние static-данные */
        totalPosts = 0,

        Post = function (post) {
            var _this = this;

            /**
             * Заголовок поста
             * @type {string}
             */
            _this.title = null;

            /**
             * Ссылка на страницу поста
             * @type {string}
             */
            _this.href = null;

            /**
             * Флаг проверки поста модератором
             * @type {boolean}
             */
            _this.isValidated = false;

            /**
             * Флаг является ли пост "звездным"
             * @type {boolean}
             */
            _this.isStarred = false;

            totalPosts++;
            _this.parseHtmlSource(post);

        },
        proto = Post.prototype;




    /**
     * Возвращает заголовок поста.
     *
     * @returns {string}
     */
    proto.getTitle = function () {
        return this.title;
    };




    /**
     * Возвращает ссылку на страницу поста.
     *
     * @returns {string}
     */
    proto.getHref = function () {
        return this.href;
    };




    /**
     * Возвращает флаг статуса проверки поста модератором.
     *
     * @returns {boolean}
     */
    proto.isApproved = function () {
        return this.isValidated;
    };




    /**
     * Формирует запись из html-фрагмента в jQuery-обертке
     *
     * @param {HTMLElement} post
     */
    proto.parseHtmlSource = function (post) {
        var
            _this = this,
            $post = $(post),
            $postHtml = $post.html();


        // Формирование заголовка
        _this.title = POST_TITLE_REGEXP.exec($post.find("h3 a").text());
        // Досрочный выход из метода если пост неподходящий
        if (_this.title === null) {
            return;
        }
        _this.title = _this.title[1];

        // Формирование адреса
        _this.href= "http://" + POST_HREF_REGEXP.exec($post.find(".b-post_comments_links").html());

        // Формирование флага проверки модератором
        _this.isValidated = POST_ISVALIDATED_REGEXP.test($postHtml);

        // Формирование флага звездности поста
        _this.isStarred = POST_ISSTARRED_REGEXP.test($postHtml);

    };



    /**
     * Возвращает HTML-код ссылки на пост.
     *
     * @returns {string}
     */
    proto.getHtmlForPostLink = function () {
        var
            _this = this,
            html = [];

        // Порция о "звездности"
        if (_this.isStarred === true) {
            html.push("<span>★&nbsp;</span>");
        }

        // Порция о "проверке модератором"
        if (_this.isValidated === true) {
            html.push("<span>✔&nbsp;</span>");
        }

        // Основная часть
        html.push("<a href='" + _this.href + "'>" + _this.title + "</a>");

        // Выпендреж с оптимизацией скорости конкатенации стрингов
        return html.join("");
    };




    return Post;
});