/**
 * Коллекция постов с набором функционала, наследуем от Array;
 */
define(function (require) {
    "use strict";
    var
        PostList = function () {
        }, _proto;

    _proto = PostList.prototype = [];




    /**
     * Сортировка коллекции постов по заголовкам, по возрастанию.
     */
    _proto.sortAscByTitle = function () {
        this.sort(function (a, b) {
            var
                aTitle = a.title.toLowerCase(),
                bTitle = b.title.toLowerCase(),
                result = 0;

            if (aTitle < bTitle) {
                result = -1
            } else if (aTitle > bTitle) {
                result = 1;
            }
            return result;
        });
    };




    /**
     * Возвращает HTML-код всех ссылок для всей коллекции.
     *
     * @returns {string}
     */
    _proto.getHtmlForCollection = function () {
        var
            html = [],
            i;

        for (i = 0; i < this.length; i++) {
            html.push(this[i].getHtmlForPostLink());
        }
        return html.join("\r\n");
    };




    return PostList;
});