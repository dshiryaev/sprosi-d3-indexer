require.config({
    baseUrl: './js/',
    paths: {
        jquery: 'lib/jquery-2.1.1.min'
    }
});


define(function (require) {
    "use strict";
    var
        $ = require("jquery"),
        App = require("App");

    window.app = App;
    $(document).ready(App.main);
});


