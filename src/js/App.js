define(function (require) {
    "use strict";
    var
        /**     Внешние зависимости     */
        Page = require("Page"),
        Post = require("Post"),
        PostList = require("PostList"),

        /** Настройки приложения */
        MAX_NUMBER_PAGES_TO_LOAD = 30,   // Максимальное количество загружаемых страниц

        /** Словарь внутренних команд */
        DO_LOAD_PAGES = "0",
        DO_PARSE_PAGES = "1",
        DO_SHOW_LINKS = "2",

        /** Словарь внутренних констант */
        LOAD_PAGES_DEBUG_TIMER_NAME = "Загрузка страниц с вебсайта-источника",
        PARSE_PAGES_DEBUG_TIMER_NAME = "Обработка загруженных страниц",

        pages = [],
        posts = new PostList(),
        $viewPort = $(".console"),
        App = {
            main: function () {
                main.call(App)
            }
        };




    /**
     * Главный поток программы
     */
    function main() {
        consolePrintLn("Начинается загрузка страниц с сайта-источника");
        doInit();
        $viewPort.trigger(DO_LOAD_PAGES);
    }



    /**
     * Инициализация приложения при запуске
     */
    function doInit() {
        // Предполагается, что в рабочих сборках отладочные выводы будут удалены сборщиком
        console.debug("Установка сокращенного числа страниц для отладки в процессе загрузки", MAX_NUMBER_PAGES_TO_LOAD = 3);

        $viewPort.on(DO_LOAD_PAGES, doCreatePages).
            on(DO_PARSE_PAGES, doParsePages).
            on(DO_SHOW_LINKS, doShowLinks);
    }




    /**
     * Обработчик события загрузки страниц
     * @param {int} pNum
     */
    function doCreatePages(pNum) {
        var
            page;

        console.time(LOAD_PAGES_DEBUG_TIMER_NAME);
        // При вызове без аргумента начинаем с начала
        if ($.isNumeric(pNum) === false) {
            pNum = 1;
        }

        if (pNum <= MAX_NUMBER_PAGES_TO_LOAD) {
            page = new Page(pNum);
            page.load(function (doFlag) {
                if (doFlag === true) {
                    consolePrintLn("Загружена страница " + pNum);
                    pages.push(page);
                    doCreatePages(pNum + 1);
                } else {
                    // Вызов парсеров
                    console.debug("Загружена последняя доступная на сервере страница");
                    $viewPort.trigger(DO_PARSE_PAGES);
                }
            });
        } else {
            $viewPort.trigger(DO_PARSE_PAGES);
        }
    }




    /**
     * Обработчик: парсинг загруженных страниц
     */
    function doParsePages() {
        var
            i, $page;

        console.timeEnd(LOAD_PAGES_DEBUG_TIMER_NAME);
        console.time(PARSE_PAGES_DEBUG_TIMER_NAME);
        for (i = 0; i < pages.length; i++) {
            $page = pages[i].$html;
            $page.find(".post").each(function () {
                var post = new Post(this);
                // Если заголовок null значит пост неподходящий и должен быть пропущен
                if (post.title !== null) {
                    posts.push(post);
                }
            });

        }

        consolePrintLn("Все страницы загружены, дождитесь вывода результата...");

        // Сортировка коллекции постов и команда на отображение
        posts.sortAscByTitle();
        console.timeEnd(PARSE_PAGES_DEBUG_TIMER_NAME);

        // Небольшая пауза для понта и переход к отображению
        setTimeout(function () {
            $viewPort.trigger(DO_SHOW_LINKS);
        }, 1500);
    }




    /**
     * Обработчик: Вывод перечня найденных ссылок на экран
     */
    function doShowLinks() {
        consoleClear();
        consolePrintLn(posts.getHtmlForCollection());
    }




    /**
     * Вывод в "консоль"
     * @param v
     */
    function consolePrintLn(v) {
        var currentContent = $viewPort.text();
        $viewPort.text(currentContent + v + "\r\n");
    }

    /**
     * Очистка "консоли"
     */
    function consoleClear() {
        $viewPort.text("");
    }



    return App;
});