/*
 * Объект-страница
 */
define(function (require) {
    "use strict";
    var
        /** Внешние зависимости модуля */
        $ = require("jquery"),

        /** Словарь констант */
        PAGE_URL = "http://sprosi.d3.ru/new/pages/",
        GETURL_API = "./api/getUrl.php",

        /**
         * Создание класса страницы с базовой инициализацией.
         *
         * @param {Number} pageNum Номер страницы
         * @constructor
         */
        Page = function (pageNum) {
            var
                _this = this;

            /**
             * Номер страницы
             * @type {Number}
             */
            _this.pageNum = null;

            /**
             * Адрес страницы
             * @type {String}
             */
            _this.pageUrl = null;

            /**
             * Текстовое содержимое страницы
             * @type {String}
             */
            _this.rawText = null;

            /**
             * $DOM-структура страницы
             *
             * @type {jQuery}
             */
            _this.$html = null;

            $.isNumeric(pageNum) ? _this.setPage(pageNum) : null;
        };




    /**
     * Установка номера страницы и формиование адреса страницы.
     *
     * @param {Number} pageNum Номер страницы
     */
    Page.prototype.setPage = function (pageNum) {
        var
            _this = this;

        _this.pageNum = pageNum;
        _this.pageUrl = PAGE_URL + pageNum;

        return pageNum;
    };




    /**
     * Загрузка страницы.
     *
     * @param {Function} callback Обработчик окончания успешного процесса загрузки
     */
    Page.prototype.load = function (callback) {
        var
            _this = this,
            dataSet = {url: _this.pageUrl};

        $.ajax({
            url: GETURL_API,
            dataType: "html",
            data: dataSet,
            type: "GET",
            success: function (data) {
                _this.rawText = data;
                _this.$html = $(data);
                if (callback && callback instanceof Function) {
                    callback(true);
                }
            },
            error: function (err) {
                _this.rawText = null;
                _this.$html = null;
                callback(false);
            }
        });
    };


    return Page;
});