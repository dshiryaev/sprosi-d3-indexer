<?php
ob_start();
$url = $_GET["url"];

$raw = file_get_contents($url);
ob_clean();
if ($raw !== false) {
	print($raw);
} else {
	header("HTTP/1.0 404 Not Found");
}
?>