module.exports = function (grunt) {
    "use strict";

    grunt.initConfig({



        /**
         * Минификатор JavaScript
         */
        uglify: {
            options: {
                mangle: {
                    sort: true,
                    toplevel: false,
                    unsafe: true,
                    except: ["require"]
                },
                compress: {
                    drop_console: false,
                    sequences: false
                }
            },
            dev: {
                options: {
                    sourceMap: true,
                    sourceMapIncludeSources: true
                },
                files: [
                    {
                        cwd: "build/js/",
                        src: ["*.js"],
                        dest: "build/js/",
                        expand: true,
                        flatten: false,
                        filter: "isFile"
                    }
                ]
            },
            rel: {
                options: {
                    sourceMap: false,
                    compress: {
                        drop_console: true
                    }
                },
                files: [
                    {
                        cwd: "build/js/",
                        src: ["*.js"],
                        dest: "build/js/",
                        expand: true,
                        flatten: false,
                        filter: "isFile"
                    }
                ]
            }
        },




        /**
         * Копировение исходников
         */
        copy: {

            /*
             * Склонировать проект из исходников в директорию сборки
             */
            cloneApp: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: "src/api/",
                        src: ["**"],
                        dest: "build/api/",
                        filter: "isFile"
                    },
                    {
                        expand: true,
                        cwd: "src/css",
                        src: ["**"],
                        dest: "build/css",
                        filter: "isFile"
                    },
                    {
                        expand: true,
                        cwd: "src/img/",
                        src: ["**"],
                        dest: "build/img/",
                        filter: "isFile"
                    },
                    {
                        expand: true,
                        cwd: "src/js/",
                        src: ["**"],
                        dest: "build/js/",
                        filter: "isFile"
                    },
                    {
                        expand: true,
                        cwd: "src/",
                        src: ["*.html"],
                        dest: "build/",
                        filter: "isFile"
                    }
                ]

            },
            cloneJs: {
                files: [
                    {
                        expand: true,
                        cwd: "src/js/",
                        src: ["**"],
                        dest: "build/js/",
                        filter: "isFile"
                    }
                ]
            },
            cloneCss: {
                files: [
                    {
                        expand: true,
                        cwd: "src/css",
                        src: ["**"],
                        dest: "build/css/",
                        filter: "isFile"
                    }
                ]
            },
            cloneHtml: {
                files: [
                    {
                        expand: true,
                        cwd: "src/",
                        src: ["*.html"],
                        dest: "build/",
                        filter: "isFile"
                    }
                ]
            }

        },




        /**
         * Очистка директории для сборки
         */
        clean: {

            /*
             * Очистить директорию сборки проекта
             */
            cleanApp: {
                src: [ "build"]
            },
            cleanJs: {
                src: ["build/js"]
            },
            cleanCss: {
                src: ["build/css"]
            },
            cleanHtml: {
                src: ["build/*.html"]
            }
        },




        shell: {
            getVersion: {
                command: "git log -1 --pretty=format:%h",
                options: {
                    callback: function log(err, stdout, strerr, cb) {
                        var
                            buildTime = (new Date()).toString();

                        grunt.file.write("build/version.json", JSON.stringify(
                            {
                                version: stdout,
                                buildTime: buildTime
                            }
                        ));

                        console.log("\r\nТекущая сборка: " + stdout);
                        cb();
                    }
                }
            }
        }
    });


    // Загрузка модулей для исполнения задач
    grunt.loadNpmTasks("grunt-contrib-uglify");  
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-shell");


    // Определение задач
    grunt.registerTask("default", ["dev"]);
    grunt.registerTask("dev", ["clean:cleanApp", "copy:cloneApp", "uglify:dev", "shell:getVersion"]);
    grunt.registerTask("release", ["clean:cleanApp", "copy:cloneApp", "uglify:rel", "shell:getVersion"]);


};